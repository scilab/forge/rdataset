// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
function [nbtotal,nberr] = readFilesInDir(datagroup)
	// Read all .dat files in the given directory.
	// nbtotal: total number of .dat files in the directory.
	// nberr: total number of .dat files which could not be read.
    dirn = rdataset_getpath (  ) + "/csv/" + datagroup;

    filemat = ls(dirn)';
    nbfiles = size(filemat,"c");
	nbtotal = 0
	nberr = 0
    for i = 1 : nbfiles
        if ( fileext(filemat(i)) == ".csv" ) then
			nbtotal = nbtotal + 1;
            dataset = basename(filemat(i));
	    mprintf("[%d] Filename: %s\n",i,filemat(i))
            instr = "data = rdataset_read(datagroup,dataset)";
            ierr = execstr(instr,"errcatch");
            if (ierr == 0 ) then
		  ;
            else
				nberr = nberr + 1;
                mprintf("ERROR !\n");
                msg = lasterror();
                mprintf("%s\n",msg);
            end
        end
    end
endfunction

stacksize("max");

mprintf("\nReading Directories\n");
dirn = rdataset_getpath (  ) + "/csv/";
dirmat = ls(dirn)';
nbdirs = size(dirmat,"c");
nbtotal=0;
nberr=0;

for n=1:nbdirs
  mprintf("[%d] Dir: %s\n",n,dirmat(n))
  datagroup=dirmat(n);
  if isdir(dirn+datagroup) then
   [nbtotal1,nberr1] = readFilesInDir(datagroup);
   nbtotal=nbtotal+nbtotal1;
   nberr=nberr+nberr1;
  end;



end;

assert_checkequal(nbtotal,597);
assert_checkequal(nberr,0);
