function sets = rdataset_listsets (datagroup  )
// Returns all available datasets of a group
// 
// Calling Sequence
//   sets = rdataset_listsets ( datagroup )
//
// Parameters
//   datagroup : string, containing the datagroup in which the dataset is located (default: "datasets")
//   sets : a vector of strings
//
// Examples
//   sets = rdataset_listsets ( "MASS" )
//
// Authors
//   Copyright (C) 2010 - 2013 - Holger Nahrstaedt


 [lhs,rhs]=argn()
    apifun_checkrhs ( "rdataset_listsets" , rhs , 0:1 )
    apifun_checklhs ( "rdataset_listsets" , lhs , 0:1 )
    
    if (rhs==0) then
       datagroup="datasets";
    end;
    if (isempty(datagroup)) then
        datagroup="datasets";
    end;
//
//
// Check type
    apifun_checktype ( "rdataset_read" , datagroup , "datagroup" , 1 , "string")
   sets = ls(rdataset_getpath()+"/csv/"+datagroup);
   for i=1:size(sets,'*');
    sets(i)=strsubst(sets(i),".csv",""); 
   end;
endfunction
